package org.rhok.foodmovr.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * The Class AbstractDao.
 * 
 * @author Maarten Cautreels
 * 
 * @param <T>
 *            the generic type
 */
public class AbstractDao<T> implements Dao<T> {

    /** The type. */
    private final Class<T> type;

    @PersistenceContext
    private EntityManager entityManager;


    /**
     * Instantiates a new dao with hibernate.
     * 
     * @param type
     *            the type
     */
    protected AbstractDao(final Class<T> type) {
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.rhok.foodmovr.dao.Dao#create(java.lang.Object)
     */
    @Override
    public T create(final T entity) {
        entityManager.persist(entity);
        return entity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.rhok.foodmovr.dao.Dao#get(int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public T get(final Long id) {
        return (T) entityManager.find(type, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.rhok.foodmovr.dao.Dao#update(java.lang.Object)
     */
    @Override
    public void update(final T entity) {
        entityManager.persist(entity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.rhok.foodmovr.dao.Dao#delete(int)
     */
    @Override
    public void delete(final Long id) {
        T entity = get(id);
        entityManager.remove(entity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.rhok.foodmovr.dao.Dao#delete(int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Collection<T> getAll() {
        return entityManager.createQuery("from " + this.type.getCanonicalName())
                .getResultList();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
