package org.rhok.foodmovr.service;

import org.rhok.foodmovr.model.exception.NotValidException;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Maarten
 * Date: 6/12/12
 * Time: 22:54
 * To change this template use File | Settings | File Templates.
 */
public interface Service<T> {
    /**
     * Creates an entity.
     *
     * @param entity The entity
     * @return Entity of type T
     * @throws org.rhok.foodmovr.model.exception.NotValidException When the entity is not valid.
     */
    T create(T entity) throws NotValidException;

    /**
     * Gets the entity by id.
     *
     * @param id The id
     * @return The entity with that id
     */
    T get(Long id);

    /**
     * Edits the entity.
     *
     * @param entity The entity
     * @throws org.rhok.foodmovr.model.exception.NotValidException When the entity is not valid.
     */
    void edit(T entity) throws NotValidException;

    /**
     * Delete an entity with that id.
     *
     * @param id The id
     */
    void delete(Long id);

    /**
     * Gets all entities.
     *
     * @return Collection of all entities
     */
    Collection<T> getAll();
}
