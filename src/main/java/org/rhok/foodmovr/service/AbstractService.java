package org.rhok.foodmovr.service;

import org.rhok.foodmovr.dao.Dao;
import org.rhok.foodmovr.model.exception.NotValidException;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Maarten
 * Date: 6/12/12
 * Time: 23:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractService<T> implements Service<T> {
    @Override
    public T create(T entity) throws NotValidException {
        return getDao().create(entity);
    }

    @Override
    public T get(Long id) {
        return getDao().get(id);
    }

    @Override
    public void edit(T entity) throws NotValidException {
        getDao().update(entity);
    }

    @Override
    public void delete(Long id) {
        getDao().delete(id);
    }

    @Override
    public Collection<T> getAll() {
        return getDao().getAll();
    }

    public abstract Dao<T> getDao();
}
