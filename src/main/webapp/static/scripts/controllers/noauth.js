'use strict';

var noAuthControllers = angular.module('noAuth.controllers', []);

noAuthControllers.controller('StaticContentCtrl', [function() {}]);

noAuthControllers.controller('ContactCtrl', ['$scope', function($scope) {
    $('.form-control-feedback').tooltip();

    $scope.sendMessage = function() {
        console.log("Sending message: ");
        console.log($scope.message);
    }
}]);